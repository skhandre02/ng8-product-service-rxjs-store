import { User } from '../models/user.model';

export const DESIGNATION_ARRAY: any = ['Java Developer', 'UI/UX Developer', 'Architect', 'HR', 'Product Manager', 'Office Boy'];

export const DISPLAY_COLUMNS: string[] = ['select','userId', 'firstName', 'lastName', 'gender', 'designation'];


export const STATIC_USER_DATA: User[] = [];

STATIC_USER_DATA.push(
  { userId: 1, firstName: 'Harry', lastName: 'Potter', gender: 'Male', designation: 'Java Developer', address: 'Khau Galli', city: 'Nashik', state: 'Maharashtra' },
  { userId: 2, firstName: 'Denis', lastName: 'Parkar', gender: 'Male', designation: 'Java Developer', address: 'Mahakali Ward', city: 'Chandrapur', state: 'Maharashtra' },
  { userId: 3, firstName: 'Jasmine', lastName: 'Alexa', gender: 'Female', designation: 'UI/UX Developer', address: 'Sadashiv Peth', city: 'Pune', state: 'Maharashtra' },
  { userId: 4, firstName: 'Chris', lastName: 'Adams', gender: 'Male', designation: 'Architect', address: 'Lane no 1.', city: 'Bangalore', state: 'Karnataka' }
);