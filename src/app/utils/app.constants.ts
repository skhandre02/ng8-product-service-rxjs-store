export const DEFAULT_LANGUAGE = "en";

export const ACTION = {
    USER_ADDED: "USER_ADDED",
    USER_EDIT: "USER_EDIT"
}