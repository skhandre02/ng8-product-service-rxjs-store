import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DEFAULT_LANGUAGE } from './utils/app.constants';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'user-management-system';
  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang(DEFAULT_LANGUAGE);
  }
}
