
import { User } from "../../models/user.model";
import { ACTION } from '../../utils/app.constants';


export function addUserReducer(state: User[] = [], action) {
  switch (action.type) {
    case ACTION.USER_ADDED:
      return [...state, action.payload];
    case ACTION.USER_EDIT:
      return [...state, action.payload];
    default:
      return state;
  }
}
