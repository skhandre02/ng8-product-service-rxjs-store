import { Component, OnInit } from '@angular/core';
import { AddUserComponent } from '../add-user/add-user.component';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { DISPLAY_COLUMNS, STATIC_USER_DATA } from '../../utils/user.config';
import { TranslateService } from '@ngx-translate/core';
import { DEFAULT_LANGUAGE, ACTION } from 'src/app/utils/app.constants';
import { Store } from '@ngrx/store';
import { UserState } from 'src/app/state/actions/user.state';
import { Observable } from 'rxjs';
import { User } from '../../models/user.model';
import { SelectionModel } from '@angular/cdk/collections';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  displayedColumns = DISPLAY_COLUMNS;

  dataSource: MatTableDataSource<User>;

  user: Observable<User[]>;

  selection = new SelectionModel<User>(true, []);

  constructor(
    private dialog: MatDialog,
    private translate: TranslateService,
    private store: Store<UserState>
  ) {
    this.translate.setDefaultLang(DEFAULT_LANGUAGE);
    this.subscribeToUserAdded();
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = STATIC_USER_DATA;
  }
  subscribeToUserAdded() {
    this.store.select("user").subscribe(data => {
      if (this.dataSource && this.dataSource.data && data && data.length > 0) {
        let type =   this.store.select("user")['actionsObserver']._value.type;
        let payload = this.store.select('user')['actionsObserver']._value.payload;
        switch(type) {
          case ACTION.USER_ADDED:
            STATIC_USER_DATA.push(payload);
            break;
          case ACTION.USER_EDIT:
            STATIC_USER_DATA.forEach
            STATIC_USER_DATA.forEach(user => {
              if (user.userId === payload.userId) {
                var indexNumber = STATIC_USER_DATA.indexOf(user);
                if (indexNumber != -1) {
                  STATIC_USER_DATA.splice(indexNumber, 1, payload);
                }
              }
            });
            break;
        }
        this.dataSource.data = STATIC_USER_DATA;
        this.selection.clear();
      }
    });
  }

  addUser() {
    const dialogRef = this.dialog.open(AddUserComponent, {
      disableClose: true
    });
  }

  deleteUser() {
    this.selection.selected.forEach(user=>{
      var indexNumber = STATIC_USER_DATA.indexOf(user);
      if (indexNumber!=-1) {
        STATIC_USER_DATA.splice(indexNumber, 1);
      }
    });
    this.dataSource.data = STATIC_USER_DATA;
    this.selection.clear();
  }

  editUser() {
    const dialogRef = this.dialog.open(AddUserComponent, {
      disableClose: true,
      data: {
        userId: this.selection.selected[0].userId,
        firstName: this.selection.selected[0].firstName,
        lastName: this.selection.selected[0].lastName,
        gender: this.selection.selected[0].gender,
        designation: this.selection.selected[0].designation,
        city: this.selection.selected[0].city,
        address: this.selection.selected[0].address,
        state: this.selection.selected[0].state
      }
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: User): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.userId + 1}`;
  }
}
