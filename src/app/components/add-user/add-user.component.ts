import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DESIGNATION_ARRAY, STATIC_USER_DATA } from '../../utils/user.config'
import { TranslateService } from '@ngx-translate/core';
import { DEFAULT_LANGUAGE, ACTION } from 'src/app/utils/app.constants';

import { UserState } from "../../state/actions/user.state";
import { Store } from '@ngrx/store';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  userForm: FormGroup;
  
  designationArray = DESIGNATION_ARRAY;
  
  isEdit:boolean = false;

  record: User; // this will be used in edit case

  constructor(public formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddUserComponent>,
    private translate: TranslateService,
    private store: Store<UserState>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.translate.setDefaultLang(DEFAULT_LANGUAGE);
    if (data && data.userId) {
      this.isEdit = true;
      this.record = data;
    }
  }

  ngOnInit(): void {
    this.createUserFormGroup();
    if (this.isEdit) {
      this.setValues();
    }
  }

  setValues() {
    if (this.record && this.isEdit) {
        this.userForm.controls.firstName.setValue(this.record.firstName);
        this.userForm.controls.lastName.setValue(this.record.lastName);
        this.userForm.controls.gender.setValue(this.record.gender);
        this.userForm.controls.designation.setValue(this.record.designation);
        this.userForm.controls.address.setValue(this.record.address);
        this.userForm.controls.city.setValue(this.record.city);
        this.userForm.controls.state.setValue(this.record.state);
    }
  }

  createUserFormGroup() {
    this.userForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      gender: ['Male'],
      designation: ['', [Validators.required]],
      address: [''],
      city: [''],
      state: ['']
    })
  }

  submitForm() {
    this.userForm.markAllAsTouched();
    if (this.userForm.valid) {
      this.store.dispatch({
        type: this.isEdit ? ACTION.USER_EDIT : ACTION.USER_ADDED,
        payload: <User>{
          userId: this.isEdit ? this.record.userId :STATIC_USER_DATA.length + 1,
          firstName: this.userForm.value.firstName,
          lastName: this.userForm.value.lastName,
          gender: this.userForm.value.gender,
          designation: this.userForm.value.designation,
          address: this.userForm.value.address,
          city: this.userForm.value.city,
          state: this.userForm.value.state
        }
      });
      this.onCancel();
      if (this.isEdit) {
        alert('User Updated Successfully!');
      } else {
        alert('User Added Successfully!');
      }
    } else {
      alert('Please fill all the mandatory details!');
    }
    console.log(this.userForm.value)
  }
  onCancel() {
    this.dialogRef.close();
  }

}
