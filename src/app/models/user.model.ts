export class User{
    userId: number;
    firstName: string;
    lastName: string;
    gender: string;
    designation: string;
    city: string;
    address: string;
    state: string;
}